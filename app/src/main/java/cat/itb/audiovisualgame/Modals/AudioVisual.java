package cat.itb.audiovisualgame.Modals;

public class AudioVisual
{
    private int audioRef;
    private int imageRef;

    public AudioVisual(int audioRef, int imageRef) {
        this.audioRef = audioRef;
        this.imageRef = imageRef;
    }

    public int getAudioRef() {
        return audioRef;
    }

    public void setAudioRef(int audioRef) {
        this.audioRef = audioRef;
    }

    public int getImageRef() {
        return imageRef;
    }

    public void setImageRef(int imageRef) {
        this.imageRef = imageRef;
    }
}
