package cat.itb.audiovisualgame;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cat.itb.audiovisualgame.Modals.AudioVisual;

public class MainActivity extends AppCompatActivity
{
    private List<AudioVisual> audioVisuals;
    private List<ImageButton> imagesViews;

    private int index = 0;
    private int correctAnswerID = 0;

    private MediaPlayer media;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        audioVisuals = new ArrayList()
        {{
            add(new AudioVisual(R.raw.bear, R.drawable.bear));
            add(new AudioVisual(R.raw.cat, R.drawable.cat));
            add(new AudioVisual(R.raw.dog, R.drawable.dog));
            add(new AudioVisual(R.raw.elephant, R.drawable.elephant));
            add(new AudioVisual(R.raw.horse, R.drawable.horse));
        }};

        imagesViews = new ArrayList()
        {{
            add(findViewById(R.id.image_button_a));
            add(findViewById(R.id.image_button_b));
            add(findViewById(R.id.image_button_c));
            add(findViewById(R.id.image_button_d));
        }};

        findViewById(R.id.play_audio_button).setOnClickListener(x -> playAudio(audioVisuals.get(index).getAudioRef()));

        for (ImageButton button : imagesViews) button.setOnClickListener(x -> selectImage(button));

        setAnswer(0);
        setImages();
    }

    private void playAudio(final int audio)
    {
        if (media != null) media.stop();
        media = MediaPlayer.create(MainActivity.this, audio);
        media.start();
    }

    private void setImages()
    {
        List<Integer> imagesUsed = new ArrayList<>();
        imagesUsed.add(audioVisuals.get(index).getImageRef());

        for (ImageButton button : imagesViews)
        {
            if (button.getId() == correctAnswerID)
                button.setImageResource(audioVisuals.get(index).getImageRef());
            else
            {
                int imgToUse = getImageDifferentFrom(imagesUsed);
                imagesUsed.add(imgToUse);

                button.setImageResource(imgToUse);
            }
        }
    }

    private int getImageDifferentFrom(final List<Integer> imagesUsed)
    {
        boolean isDifferent;
        for (AudioVisual avd : audioVisuals)
        {
            isDifferent = true;
            int imageRef = avd.getImageRef();
            for (int usedImage : imagesUsed)
            {
                if (imageRef == usedImage)
                {
                    isDifferent = false;
                    break;
                }
            }
            if (isDifferent) return imageRef;
        }
        return R.drawable.bruh;
    }

    private void selectImage(final ImageButton button)
    {
        playAudio(button.getId() == correctAnswerID ? R.raw.won : R.raw.lost);
        setAnswer((index + 1) % audioVisuals.size());
        setImages();
    }

    private void setAnswer(final int indexPos)
    {
        if (indexPos == 0) Collections.shuffle(audioVisuals);
        index = indexPos;
        Collections.shuffle(imagesViews);
        correctAnswerID = imagesViews.get(0).getId();
    }
}